# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from datetime import datetime
import re


SLACK_TOKEN = "xoxb-675616793778-678313658978-SOYBHOpbbW3tgL5zLORBG2Fe"
SLACK_SIGNING_SECRET = "e3ea44af2819f764784b2d14f0d20c8f"

app = Flask(__name__)
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
now = datetime.now()

def craw():
        list_link = []
        list_title=[]
        url = "https://sports.news.naver.com/kfootball/news/index.nhn?isphoto=N"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")


        for data in soup.find_all("a", class_="title") :
                list_title.append(data.get_text())
                list_link.append("https://sports.news.naver.com"+data.get('href'))

        return list_link,list_title

def get_data(s_date,e_date, w_h_t, w_a_t):
    source_code = urllib.request.urlopen("https://search.naver.com/search.naver?sm=tab_hty.top&where=nexearch&query=%EC%B6%95%EA%B5%AC&oquery=%EC%B6%95%EA%B5%AC&tqi=Ufkk1dp0J14ssKwViI0ssssstBV-373346").read()
    soup = BeautifulSoup(source_code, "html.parser")

    date =[]
    time = []
    h_t =[]
    a_t =[]
    score = []
    date_count= 0
    #print(soup.find_all("div", class_="data-body"))
    #date.append(soup.find("td",class_="play_date first").get_text().replace("\n","").replace(" ",""))
    for data in soup.find_all("tbody", class_="_scroll_content"):
        for a in data.find_all("tr"):
            h_t.append([])
            a_t.append([])
            time.append([])
            score.append([])
            if (a.find_all("td",class_="play_date")) : 
                [date.append(x.get_text().replace("\n","").replace(" ","")) for x in a.find_all("td",class_="play_date")]
                date_count+=1
            else:
                [h_t[date_count].append(x.get_text().replace("\n","")) for x in a.find_all("em",class_="team_lft")]
                [a_t[date_count].append(x.get_text().replace("\n","")) for x in a.find_all("em",class_="team_rgt")]
                [time[date_count].append(x.get_text().replace("\n","").replace(" ","")) for x in a.find_all("td",class_="time")]
                [score[date_count].append(x.get_text().replace("\n","").replace(" ","")) for x in a.find_all("em",class_="txt_score")]
    h_t.remove([])
    a_t.remove([])
    time.remove([])
    score.remove([])

    result = []
    cur_count =0
    for i in range(len(date)):
        for j in range(len(h_t[i])):
            result.append([])
            result[cur_count].append(date[i])
            result[cur_count].append(time[i][j])
            result[cur_count].append(h_t[i][j])
            result[cur_count].append(a_t[i][j])
            result[cur_count].append(score[i][j])
            cur_count += 1

    what_week = re.compile("(\D)")
    what_day = re.compile(".\d{2}")
    '''
    
    match_text = compile_text.findall(sentence)
    '''
    temp_result = []

    for i in range(len(result)):
        if w_h_t == "all" : break
        if w_h_t == result[i][2] : temp_result.append(result[i])

    for i in range(len(result)):
        if w_a_t == "all" : break
        if w_a_t == result[i][3] : temp_result.append(result[i])

    final_result = []
    t_d = []
    v_s = []
    for data in temp_result: 
        standard_Day = int(str(what_day.findall(data[0]))[3:5])
        if((standard_Day >=int(s_date)) & (standard_Day <= int(e_date)) ):
            t_d_ele = data[1]+"|"+data[0]
            v_s_ele = data[2]+data[4]+data[3]
            t_d.append(t_d_ele)
            v_s.append(v_s_ele)
    
    #날짜 시간 대진표 경기장
    

 

    #(int w_date, w_h_t, w_a_t)
    #s_date,e_date, w_h_t, w_a_t

    return ('\n'.join(t_d)),('\n'.join(v_s))


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    print(text)
    
    if '뉴스' in text:
        link,title = craw()
        for i in range(10):
                slack_web_client.chat_postMessage(
                        channel=channel,
                        text=str(i+1)+"위 : "+"<"+link[i]+"|"+title[i]+">"
                )
        
#-------------- 메뉴 표시-------------
    else:
        temp = text.split(' ')
        week = temp[1]
        home = temp[2]
        away = temp[3]

        if '경기' in away:
                away = 'all'

        if '이번주' in week:
                first_w = int(now.day) -5
                last_w = int(now.day)
        elif '다음주' in week:
                first_w = int(now.day)
                last_w = int(now.day) +5


       # print(type(first_w)
        temp_data = get_data(first_w,last_w,home,away)
        temp_data1 = get_data(int(first_w),int(last_w),away,home)
        menu=[]
        menu.append("*날짜*")
        menu.append("*시간*")
        menu.append("*대진표*")
        menu.append("*경기장*")

        for i in range(2):
                menu[i] = menu[i].center(19,"-")
        for i in range(2):
                menu[i+2] = menu[i+2].center(17,"-")
        
        m1 = menu[0]+"|"+menu[1]
        m2 = menu[2]+"|"+menu[3]
        menu_content = SectionBlock(
                #self.fields_max_length = 3,
                fields=[m1,m2]

        #item1,item2 = get_data(13,27,"대구","대구")
        )
        t1 = temp_data[0]+('\n')+(temp_data1[0])
        t2 = temp_data[1]+('\n')+(temp_data1[1])
        content = SectionBlock(
                #self.fields_max_length = 3,
                fields=[t1,t2]
        )


        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [menu_content, content]
        slack_web_client.chat_postMessage(
                channel=channel,
                #blocks = block1,
                blocks=extract_json(my_blocks)
        )
    

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
